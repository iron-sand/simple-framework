﻿global using Simple.Core.Models;
global using Simple.Core.Models.Entity;
global using Simple.Contracts;
global using Simple.Core.Web;
global using Simple.Core.Attributes;
global using Simple.Core;
global using Simple.Core.Helper;